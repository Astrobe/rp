-- Author: Astrobe
-- licence: CC-BY-SA 4.0
-- Version 2.1
-- v2.1 changes:
-- Little API: rp.award() let other mods givve/take RPs.
-- v2 changes:
-- Reputation can go negative
-- Better description
-- RP donations are no longer broadcasted
-- messages are colored in yellow
-- the donor is not named

local reputations=minetest.get_mod_storage()

rp={}

-- Award 'amount' of RP to player 'name'.
-- Amount can be negative
-- 'allowneg' tells the function if it is ok that the RP of the player goes negative.
-- if not, then function does not perform the operation and returns false.
-- if 'name' is unknown, the function also returns true

function rp.award(name, amount, allowneg)
	local current=reputations:get_int(name)
	if not current then return false end
	local new=current+amount
	if new<0 and not allowneg then return false end
	reputations:set_int(name, new)
	return true
end


minetest.register_privilege("rep", {"give/take reputation points freely", false})

minetest.register_chatcommand("rep", {
	description="Give or view reputation points (RPs). If a negative number of RPs is given, the amount is subtracted from the player's RPs (bad reputation).\n In all cases, the points are taken from your own RPs.",
	params="<name> <number> (give RPs) or <name> (view RPs)",
	func=function(name, param)
		local receiver, amount=string.match(param,"^([%a%d_-]+) (-?%d+)")
		if not amount then
			receiver=string.match(param,"[%a%d_-]+")
		end
		if not receiver then
			minetest.chat_send_player(name, "Your current RPs: "..(reputations:get_int(name) or 0))
			return
		end
		if not minetest.player_exists(receiver) then
			minetest.chat_send_player(name, "Who?")
			return
		end
		local has_rep_priv=minetest.check_player_privs(name, "rep")
		if receiver and amount then
			if name==receiver then
				minetest.chat_send_player(name, "Nice try...")
				return
			end
			local n=tonumber(amount)
			local nr=reputations:get_int(receiver) or 0
			local ng=reputations:get_int(name) or 0
			if n>ng and not has_rep_priv then
				minetest.chat_send_player(name,"You don't have enough RPs")
				return
			end
			nr=nr+n
			-- if not has_rep_priv then
			-- Let admins have their rp go negative if they want
				ng=ng-math.abs(n)
			-- end
			reputations:set_int(receiver, nr)
			reputations:set_int(name, ng)
			minetest.chat_send_player(receiver, minetest.colorize("yellow","You received "..n.." RPs"))
			minetest.chat_send_player(name, minetest.colorize("yellow","You gave "..n.." RPs to ".. receiver))
			return
		end
		if receiver then
			rps=reputations:get_int(receiver)
			minetest.chat_send_player(name, receiver.." has "..rps.." RP")
			if minetest.check_player_privs(receiver, "rep") then
				minetest.chat_send_player(name, receiver.." has 'rep' privilege")
			end
		end
	end})
